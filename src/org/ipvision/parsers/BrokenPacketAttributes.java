/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.parsers;

/**
 *
 * @author Ashraful
 */
public class BrokenPacketAttributes {

    private String clientPacketID;
    private int serverPacketID;
    private int action;
    private int totalPacket;
    private int packetNumber;
    private String clientUniqueKey;
    private long serverUniqueKey;
    private byte[] data;
    private String userID;
    private String webKey;
    private int tabID;

    public String getClientPacketID() {
        return clientPacketID;
    }

    public void setClientPacketID(String clientPacketID) {
        this.clientPacketID = clientPacketID;
    }

    public int getServerPacketID() {
        return serverPacketID;
    }

    public void setServerPacketID(int serverPacketID) {
        this.serverPacketID = serverPacketID;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public int getTotalPacket() {
        return totalPacket;
    }

    public void setTotalPacket(int totalPacket) {
        this.totalPacket = totalPacket;
    }

    public int getPacketNumber() {
        return packetNumber;
    }

    public void setPacketNumber(int packetNumber) {
        this.packetNumber = packetNumber;
    }

    public String getClientUniqueKey() {
        return clientUniqueKey;
    }

    public void setClientUniqueKey(String uniqueKey) {
        this.clientUniqueKey = uniqueKey;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public long getServerUniqueKey() {
        return serverUniqueKey;
    }

    public void setServerUniqueKey(long serverUniqueKey) {
        this.serverUniqueKey = serverUniqueKey;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getWebKey() {
        return webKey;
    }

    public void setWebKey(String webKey) {
        this.webKey = webKey;
    }

    public int getTabID() {
        return tabID;
    }

    public void setTabID(int tabID) {
        this.tabID = tabID;
    }

    @Override
    public String toString() {
        return "packetID:" + clientPacketID + " action:" + action + " totalPacket:" + totalPacket + " packetNumber:" + packetNumber + " uniqueKey:" + clientUniqueKey + " serverPacketID:" + serverPacketID + " serverUniqueKey:" + serverUniqueKey;
    }
}
