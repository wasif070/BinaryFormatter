/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.parsers;

import java.util.ArrayList;

/**
 *
 * @author Ashraful
 */
public class RelayPacketAttributes {

    private int action;
    private int originAction;
    private long packetID;
    private int serverID;
    private String clientPacketID;
    private long userID;
    private long friendID;
    private String userIdentity;
    private String friendIdentity;
    private String userName;
    private String friendName;
    private int device;
    private int presence;
    private int mood;
    private String callID;
    private String shortCode;
    private int callType;
    private byte[] data;
    private int isDevidedPacket;
    private long switchIP;
    private int switchPort;
    /* call forwarding/transferring */
    private long tranferByID;
    private String tranferByIdentity;
    private String tranferByName;
    private long divertedByID;
    private String divertedByIdentity;
    private String divertedByName;
    private long ringID;
    private int p2pStatus;
    private int deviceCategory;
    private boolean isCelebrity;
    private int remotePushType = 1;
    private ArrayList<Long> userIDList;
    private String roomID;
    private long shadowID;

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public int getOriginAction() {
        return originAction;
    }

    public void setOriginAction(int originAction) {
        this.originAction = originAction;
    }

    public long getServerPacketID() {
        return packetID;
    }

    public void setServerPacketID(long packetID) {
        this.packetID = packetID;
    }

    public int getServerID() {
        return serverID;
    }

    public void setServerID(int serverID) {
        this.serverID = serverID;
    }

    public String getClientPacketID() {
        return clientPacketID;
    }

    public void setClientPacketID(String packetID) {
        this.clientPacketID = packetID;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public long getFriendID() {
        return friendID;
    }

    public void setFriendID(long friendID) {
        this.friendID = friendID;
    }

    public int getDevice() {
        return device;
    }

    public void setDevice(int device) {
        this.device = device;
    }

    public int getLiveStatus() {
        return presence;
    }

    public void setLiveStatus(int presence) {
        this.presence = presence;
    }

    public int getMood() {
        return mood;
    }

    public void setMood(int mood) {
        this.mood = mood;
    }

    public String getCallID() {
        return callID;
    }

    public void setCallID(String callID) {
        this.callID = callID;
    }

    public int getCallType() {
        return callType;
    }

    public void setCallType(int callType) {
        this.callType = callType;
    }

    public String getFriendIdentity() {
        return friendIdentity;
    }

    public void setFriendIdentity(String friendRingID) {
        this.friendIdentity = friendRingID;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public int getIsDevidedPacket() {
        return isDevidedPacket;
    }

    public void setIsDevidedPacket(int isDevidedPacket) {
        this.isDevidedPacket = isDevidedPacket;
    }

    public String getUserIdentity() {
        return userIdentity;
    }

    public void setUserIdentity(String userIdentity) {
        this.userIdentity = userIdentity;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public long getSwitchIP() {
        return switchIP;
    }

    public void setSwitchIP(long switchIP) {
        this.switchIP = switchIP;
    }

    public int getSwitchPort() {
        return switchPort;
    }

    public void setSwitchPort(int switchPort) {
        this.switchPort = switchPort;
    }

    /* call related methods */
    public long getTranferByID() {
        return tranferByID;
    }

    public void setTranferByID(long tranferByID) {
        this.tranferByID = tranferByID;
    }

    public String getTranferByIdentity() {
        return tranferByIdentity;
    }

    public void setTranferByIdentity(String tranferByIdentity) {
        this.tranferByIdentity = tranferByIdentity;
    }

    public String getTranferByName() {
        return tranferByName;
    }

    public void setTranferByName(String tranferByName) {
        this.tranferByName = tranferByName;
    }

    public long getDivertedByID() {
        return divertedByID;
    }

    public void setDivertedByID(long divertedByID) {
        this.divertedByID = divertedByID;
    }

    public String getDivertedByIdentity() {
        return divertedByIdentity;
    }

    public void setDivertedByIdentity(String divertedByIdentity) {
        this.divertedByIdentity = divertedByIdentity;
    }

    public String getDivertedByName() {
        return divertedByName;
    }

    public void setDivertedByName(String divertedByName) {
        this.divertedByName = divertedByName;
    }

    public long getRingID() {
        return ringID;
    }

    public void setRingID(long ringID) {
        this.ringID = ringID;
    }

    public int getP2PStatus() {
        return p2pStatus;
    }

    public void setP2PStatus(int p2pStatus) {
        this.p2pStatus = p2pStatus;
    }

    public String getCountryShortCode() {
        return shortCode;
    }

    public void setCountryShortCode(String countryShortCode) {
        this.shortCode = countryShortCode;
    }

    public int getDeviceCategory() {
        return deviceCategory;
    }

    public void setDeviceCategory(int deviceCategory) {
        this.deviceCategory = deviceCategory;
    }

    public boolean isIsCelebrity() {
        return isCelebrity;
    }

    public void setIsCelebrity(boolean isCelebrity) {
        this.isCelebrity = isCelebrity;
    }

    public int getRemotePushType() {
        return remotePushType;
    }

    public void setRemotePushType(int remotePushType) {
        this.remotePushType = remotePushType;
    }

    public ArrayList<Long> getUserIDList() {
        return userIDList;
    }

    public void setUserIDList(ArrayList<Long> userIDList) {
        this.userIDList = userIDList;
    }

    public String getRoomID() {
        return roomID;
    }

    public void setRoomID(String roomID) {
        this.roomID = roomID;
    }

    public long getShadowID() {
        return shadowID;
    }

    public void setShadowID(long shadowID) {
        this.shadowID = shadowID;
    }
}
