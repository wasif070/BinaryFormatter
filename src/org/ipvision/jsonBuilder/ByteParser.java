/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.jsonBuilder;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import org.ringid.utilities.AppConstants;
import org.ringid.utilities.AttributeCodes;

/**
 *
 * @author saikat
 */


public class ByteParser {

    private byte[] bytes;

    private JsonObject jsonRespose;

    private int data_length;

    private int full_data_length = 0;

    private int index;

    private int totalPacket;

    private ArrayList<byte[]> brokenPackets;

    private boolean isBrokenPacket = false;

    private int jsonCheckByte;

    public ByteParser(byte[] data, int length) {

        this.bytes = data;
        this.full_data_length = length;

        this.jsonRespose = new JsonObject();

    }

    public ByteParser(ArrayList<byte[]> brokenPackets) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        this.brokenPackets = brokenPackets;
        isBrokenPacket = true;
    }

    public Object getValueByAction(int givenCode) {

        Object value = null;
        if (!isBrokenPacket) {
            index = 1;
        } else {

            makeFullPacket();
            index = 0;
        }

        while (index < full_data_length) {

            int attributeCode = getInt(2);
            int attrLength = (bytes[index++] & 0xFF);

            ATTRIBUTE_CODE_INFO attribute_code_info = ATTRIBUTE_CODE_INFO.get(attributeCode);
            if (attribute_code_info == null) {
                //System.out.println("Attribute : " + attributeCode + "not found ");
                break;
            } else {
                String type = attribute_code_info.getType();
                switch (type) {
                    case "bool": {
                        value = getBool(attrLength);
                        break;

                    }
                    case "int": {
                        value = getInt(attrLength);
                        break;
                    }

                    case "string": {
                        value = getString(attrLength);
                        break;
                    }

                    case "long": {
                        value = getLong(attrLength);
                        break;
                    }
                    case "double": {
                        value = Double.parseDouble(getString(attrLength));
                        break;
                    }

                    case "byte": {
//                            int length = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
                        index++;
//                            System.out.println("this attribute is an array ");
                        break;
                    }

                }

                if (attributeCode == givenCode) {
                    return value;
                }

            }

        }

        return "Attribute " + givenCode + " not found";
    }

    public JsonObject getJsonObject() {

        if (!isBrokenPacket) {

            index = 1;
            jsonCheckByte = 0;
        } else {

            jsonCheckByte = 1;
            makeFullPacket();
            index = 0;
        }

        jsonRespose = deserialzeJson(full_data_length);
        jsonRespose.addProperty("chk", jsonCheckByte);

        return jsonRespose;
    }

    private JsonObject deserialzeJson(int iterationLength) {

        JsonObject jsonObject = new JsonObject();

        // JsonArray jsonArray = new JsonArray();
        ArrayList<Object> arrayList = new ArrayList<>();

        boolean duplicacyFlag = false;
        String shortName = null;
        String duplicateAttributeShortName = null;

        while (index < iterationLength) {

            int attributeCode = getInt(2);

            //ATTRIBUTE_CODE_INFO.get(attributeCode).getSize();
            // BYTE_SIZE byteSize = ATTRIBUTE_CODE_INFO.get(attributeCode).getSize();
            ATTRIBUTE_CODE_INFO attributeCodeInfo = ATTRIBUTE_CODE_INFO.get(attributeCode);
            if (attributeCodeInfo == null) {
//                System.out.println("Missing attribute code: " + attributeCode);
                int missingAttrLen = (bytes[index++] & 0xFF);
                index += missingAttrLen;
                continue;
            }

            String type = attributeCodeInfo.getType();
            shortName = attributeCodeInfo.getName();

            if (jsonObject.has(shortName)) {

                duplicacyFlag = true;
                arrayList.add(jsonObject.get(shortName));
                //jsonObject.remove(shortName);
                duplicateAttributeShortName = shortName;

            } else {
                if (duplicacyFlag == true) {
                    jsonObject.remove(duplicateAttributeShortName);
                    jsonObject.addProperty(duplicateAttributeShortName, new Gson().toJson(arrayList));
                    duplicacyFlag = false;
                    arrayList.clear();

                }
                switch (type) {

                    case "bool": {

                        int length = (bytes[index++] & 0xFF);
                        boolean value = getBool(length);
                        if (duplicacyFlag) {
                            arrayList.add(value);

                        } else {
                            jsonObject.addProperty(shortName, value);
                        }

                        break;

                    }

                    case "int": {
                        int length = (bytes[index++] & 0xFF);
                        int value = getInt(length);
                        if (duplicacyFlag) {
                            arrayList.add(value);

                        } else {
                            jsonObject.addProperty(shortName, value);
                        }
                        break;

                    }

                    case "string": {
                        int length = (bytes[index++] & 0xFF);
                        String value = getString(length);
                        if (duplicacyFlag) {
                            arrayList.add(value);
                        } else {
                            jsonObject.addProperty(shortName, value);
                        }
                        break;
                    }

                    case "long": {
                        int length = (bytes[index++] & 0xFF);
                        long value = getLong(length);
                        if (duplicacyFlag) {
                            arrayList.add(value);
                        } else {
                            jsonObject.addProperty(shortName, value);
                        }
                        break;
                    }

                    case "double": {
                        int length = (bytes[index++] & 0xFF);
                        double value = Double.parseDouble(getString(length));
                        if (duplicacyFlag) {

                            arrayList.add(value);

                        } else {
                            jsonObject.addProperty(shortName, value);
                        }
                        break;
                    }

                    case "byte": {

                        int length = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
                        JsonObject jo = deserialzeJson(length + index - 1);
                        if (duplicacyFlag) {

                            arrayList.add(jo);
                        } else {
                            jsonObject.add(shortName, jo);
                        }

                        break;
                    }

                    case "bigString": {
                        int length = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
                        String value = getString(length);
                        if (duplicacyFlag) {
                            arrayList.add(value);
                        } else {
                            jsonObject.addProperty(shortName, value);
                        }
                        break;
                    }
                    default: {

                        //index += length;
                        break;
                    }
                }

            }
        }

        if (!arrayList.isEmpty()) {
            jsonObject.remove(duplicateAttributeShortName);
            jsonObject.addProperty(shortName, new Gson().toJson(arrayList));
        }
        return jsonObject;

    }

    private void brokenPacketHeaderParse() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        while (true) {

            int attributeCode = bytes[index++] & 0xFF;

            if (attributeCode == AttributeCodes.DATA) {
                break;
            }

            switch (attributeCode) {

                case AttributeCodes.SERVER_PACKET_ID: {
                    int length = bytes[index++] & 0xFF;
                    long value = getLong(length);
                    // jsonObject.addProperty(attributeinfos.SERVER_PACKET_ID.getName(), value);
                    break;
                }

                case AttributeCodes.ACTION: {
                    int length = bytes[index++] & 0xFF;
                    int value = getInt(length);
                    // jsonObject.addProperty(attributeinfos.ACTION.getName(), value);
                    break;
                }

                case AttributeCodes.CLIENT_PACKET_ID: {
                    int length = bytes[index++] & 0xFF;
                    String value = getString(length);
                    // jsonObject.addProperty(attributeinfos.CLIENT_PACKET_ID.getName(), value);
                    break;
                }

                case AttributeCodes.WEB_UNIQUE_KEY: {
                    int length = bytes[index++] & 0xFF;
                    String value = getString(length);
                    //jsonObject.addProperty(attributeinfos.WEB_UNIQUE_KEY.getName(), value);
                    break;
                }

                case AttributeCodes.WEB_TAB_ID: {
                    int length = bytes[index++] & 0xFF;
                    int value = getInt(length);
                    //jsonObject.addProperty(attributeinfos.WEB_TAB_ID.getName(), value);
                    break;
                }

                case AttributeCodes.UNIQUE_KEY: {
                    int length = bytes[index++] & 0xFF;
                    int value = getInt(length);
                    // jsonObject.addProperty(attributeinfos.UNIQUE_KEY.getName(), value);
                    break;
                }

                case AttributeCodes.PACKET_NUMBER: {
                    int length = bytes[index++] & 0xFF;
                    int value = getInt(length);
                    //jsonObject.addProperty(attributeinfos.PACKET_NUMBER.getName(), value);
                    break;
                }

                case AttributeCodes.TOTAL_PACKETS: {
                    int length = bytes[index++] & 0xFF;
                    totalPacket = getInt(length);
                    //jsonObject.addProperty(attributeinfos.TOTAL_PACKET.getName(), value);
                    break;
                }

            }
        }

        data_length = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);

    }

    private void makeFullPacket() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

        byte[] tempPacket = new byte[AppConstants.TWO_KB];
        int tempPacketIndex = 0;

        for (byte[] brokenPacket : brokenPackets) {

            index = 1;
            bytes = brokenPacket;
            brokenPacketHeaderParse();
            System.arraycopy(bytes, index, tempPacket, tempPacketIndex, data_length);
            tempPacketIndex += data_length;
            full_data_length += data_length;

        }

        bytes = tempPacket;

    }

    private int getInt(int length) {
        switch (length) {
            case 1:
                return (bytes[index++] & 0xFF);

            case 2:
                return (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
            case 4:
                return (bytes[index++] & 0xFF) << 24 | (bytes[index++] & 0xFF) << 16 | (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
        }
        return 0;
    }

    private long getLong(int length) {
        long result = 0;
        for (int i = (length - 1); i > -1; i--) {
            result |= (bytes[index++] & 0xFFL) << (8 * i);
        }
        return result;
    }

    private String getString(int length) {
        byte[] str_bytes = new byte[length];
        System.arraycopy(bytes, index, str_bytes, 0, length);
        index += length;
        return new String(str_bytes);
    }

    private byte[] getBytes(int length) {
        byte[] data_bytes = new byte[length];
        if (bytes.length - index >= length) {
            System.arraycopy(bytes, index, data_bytes, 0, length);
        }
        index += length;
        return data_bytes;
    }

    private boolean getBool(int length) {
        return (bytes[index++] != 0);
    }

}
