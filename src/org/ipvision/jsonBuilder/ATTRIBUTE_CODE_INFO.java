/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.jsonBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author saikat
 */
public enum ATTRIBUTE_CODE_INFO {
    
    ACTION("actn",                  1, "int",  BYTE_SIZE.TWO_BYTES),
    SERVER_PACKET_ID("pckFs",        2,  "long", BYTE_SIZE.EIGHT_BYTES),
    SESSION_ID("sId",                3, "string",  BYTE_SIZE.UNDEFINED),
    TOTAL_PACKETS("tPack",           4, "int",  BYTE_SIZE.TWO_BYTES),
    PACKET_NUMBER("cPack",           5, "int",  BYTE_SIZE.TWO_BYTES),
    USER_ID("utId",                  7,"long", BYTE_SIZE.EIGHT_BYTES),
    FRIEND_ID("fId",                8, "long" , BYTE_SIZE.SIX_BYTES),
    DEVICE_TYPE("dvc",              9,"int", BYTE_SIZE.FOUR_BYTES),
    CLIENT_PACKET_ID("pckId",       10, "string", BYTE_SIZE.UNDEFINED),
    CALL_ID("callID",               11, "string", BYTE_SIZE.UNDEFINED),
    FRIEND_IDENTITY("fndID",        12, "long", BYTE_SIZE.UNDEFINED),
    CALL_TIME("tm",                 13, "long", BYTE_SIZE.EIGHT_BYTES),
    PRESENCE("psnc",                14,"int", BYTE_SIZE.FOUR_BYTES),
    USER_IDENTITY("uId",            16,"long", BYTE_SIZE.EIGHT_BYTES),
    USER_NAME("fn",                 17, "string", BYTE_SIZE.UNDEFINED),
    TOTAL_RECORDS("tr",             18, "int", BYTE_SIZE.TWO_BYTES),
    SUCCESS("sucs",                 20, "bool", BYTE_SIZE.ONE_BYTE),
    MESSAGE("mg",                   21,  "string", BYTE_SIZE.UNDEFINED),
    MESSAGE2("msg",                 21,"string", BYTE_SIZE.UNDEFINED),
    REASON_CODE("rc",               22, "int", BYTE_SIZE.TWO_BYTES),
    STATUS("ists",                  23,"int",BYTE_SIZE.FOUR_BYTES),
   // DELETED("del",                  24,  BYTE_SIZE.ONE_BYTE),
    SWITCH_IP("swIp",               25, "string", BYTE_SIZE.UNDEFINED),
    SWITCH_PORT("swPr",             26, "int", BYTE_SIZE.FOUR_BYTES),
    MOOD("mood",                    38, "int", BYTE_SIZE.TWO_BYTES),
    CALL_TYPE("callT",              41, "int", BYTE_SIZE.TWO_BYTES),
    P2P_STATUS("p2p",               42, "int", BYTE_SIZE.FOUR_BYTES),
    IS_CELEBRITY("isclb",           45, "bool", BYTE_SIZE.ONE_BYTE),
    REMOTE_PUSH_TYPE("rpt",         48, "int", BYTE_SIZE.FOUR_BYTES),
    CONTACT("contactList",          101,"byte", BYTE_SIZE.UNDEFINED),
    CONTACT_TYPE("ct",              102, "int",BYTE_SIZE.UNDEFINED),
    FRIENDSHIP_STATUS("frnS",       104,"int", BYTE_SIZE.TWO_BYTES),
    BLOCK_VALUE("bv",               105,"int", BYTE_SIZE.TWO_BYTES),
    CONTACT_UPDATE_TIME("cut",      107, "long",BYTE_SIZE.EIGHT_BYTES),
    MUTUAL_FRIEND_COUNT("mfc",      108,"int", BYTE_SIZE.FOUR_BYTES),
    CALL_ACCESS("cla",              110,"int", BYTE_SIZE.ONE_BYTE),
    CHAT_ACCESS("chta",             111,"int", BYTE_SIZE.ONE_BYTE),
    FEED_ACCESS("fda",              112,"int", BYTE_SIZE.ONE_BYTE),
    PASSWORD("usrPw",               128,"string", BYTE_SIZE.UNDEFINED),
    EMAIL("el",                     130, "string", BYTE_SIZE.UNDEFINED),
    MOBILE_PHONE("mbl",             133, "string", BYTE_SIZE.UNDEFINED),
    DEVICE_TOKEN("dt",              132, "string", BYTE_SIZE.UNDEFINED),
    BIRTH_DATE("bDay",              134, "long", BYTE_SIZE.UNDEFINED),
    MARRIAGE_DAY("mDay",            135,"long", BYTE_SIZE.UNDEFINED),
    GENDER("gr",                    136, "string", BYTE_SIZE.UNDEFINED),
    COUNTRY_ID("cnId",               137,"int", BYTE_SIZE.FOUR_BYTES),
   // CURRENT_CITY("cc",              138, BYTE_SIZE.UNDEFINED),
   // HOME_CITY("hc",                 139, BYTE_SIZE.UNDEFINED),
    DIALING_CODE("mblDc",           142, "string", BYTE_SIZE.UNDEFINED),
    IS_MY_NUMBER_VERIFIED("imnv",   143,"int", BYTE_SIZE.ONE_BYTE),
    IS_EMAIL_VERIFIED("iev",        145,"int", BYTE_SIZE.TWO_BYTES),
    MY_NUMBER_VERIFICATION_CODE("vc",   146,"string", BYTE_SIZE.UNDEFINED),
    EMAIL_VERIFICATION_CODE("evc",   148,"string", BYTE_SIZE.UNDEFINED),
    PROFILE_IMAGE("prIm",           151, "string", BYTE_SIZE.UNDEFINED),
    PROFILE_IMAGE_ID("prImId",      152, "long", BYTE_SIZE.SIX_BYTES),
    COVER_IMAGE("cIm",              153, "string", BYTE_SIZE.UNDEFINED),
    COVER_IMAGE_ID("cImId",         154, "long", BYTE_SIZE.SIX_BYTES),
    COVER_IMAGE_X("cimX",           155, "int", BYTE_SIZE.TWO_BYTES),
    COVER_IMAGE_Y("cimY",           156, "int", BYTE_SIZE.TWO_BYTES),
   // ABOUT_ME("am",                  157, BYTE_SIZE.UNDEFINED),
    TOTAL_FRIENDS("tf",             158,"int", BYTE_SIZE.TWO_BYTES),
   // RING_EMAIL("re",                159, BYTE_SIZE.UNDEFINED),
    UPDATE_TIME("ut",               160, "long", BYTE_SIZE.UNDEFINED),
    NOTIFICATION_VALIDITY("nvldt",  161,"int", BYTE_SIZE.ONE_BYTE),
    WEB_LOGIN_ENABLED("wle",        162,"int", BYTE_SIZE.ONE_BYTE),
    PC_LOGIN_ENABLED("ple",         163,"int",BYTE_SIZE.ONE_BYTE),
    //COMMON_FRIEND_SUGGESTION("cfs", 164, BYTE_SIZE.UNDEFINED),
   // PHONE_NUMBER_SUGGESTION("pns",  165, BYTE_SIZE.UNDEFINED),
    //CONTACT_LIST_SUGGESTION("cls",  166, BYTE_SIZE.UNDEFINED),
    //new attributes
    COUNTRY("cnty",                 169,"string", BYTE_SIZE.UNDEFINED),
    VERSION_MESSAGE("vsnMsg",       170, "string",BYTE_SIZE.UNDEFINED),
    DOWNLOAD_MANDATORY("dwnMnd",    171, "bool", BYTE_SIZE.ONE_BYTE),
    EMOTICON_VERSION("emVsn",       172,"double",BYTE_SIZE.UNDEFINED),
    PRIVACY("pvc",                  173,"int", BYTE_SIZE.FOUR_BYTES),
    USER_TABLE_ID("utId",           174,"long", BYTE_SIZE.SIX_BYTES),
    MAIL_SERVER_IP("mailIP",        175,"string",BYTE_SIZE.UNDEFINED),
    SMTP_PORT("smtpPrt",            176,"int",BYTE_SIZE.FOUR_BYTES),
    IMAP_PORT("imapPrt",            177,"int",BYTE_SIZE.UNDEFINED),
    IMAGE_SERVER_IP("imgIP",        178, "string",BYTE_SIZE.UNDEFINED),
    MARKET_SREVER_IP("mrktIP",      179, "string",BYTE_SIZE.UNDEFINED),
    OFFLINE_SERVER_PORT("oPrt",     180,"int",BYTE_SIZE.FOUR_BYTES),
    OFFLINE_SERVER_IP("oIP",        181,"string",BYTE_SIZE.UNDEFINED),
    NAME("nm",                      182,"string",BYTE_SIZE.UNDEFINED),
    USER_ID_CHANGE("uIdChng",       183, "bool", BYTE_SIZE.ONE_BYTE),
   // ADDITIONAL_INFO("adInfo",       184, BYTE_SIZE.UNDEFINED),
    USER_FOUND("uf",                185, "int", BYTE_SIZE.ONE_BYTE),
    PASSWORD_SETED("pstd",          186, "bool", BYTE_SIZE.ONE_BYTE),
    LIVE_STATUS("lsts",             187,"int", BYTE_SIZE.FOUR_BYTES),
    CALL_FORWARD_TO("cft",          189, "long", BYTE_SIZE.UNDEFINED),
   // PUSH_ENABLE("pen",              190, BYTE_SIZE.ONE_BYTE),
    ANONYMOUS_CALL("anc",           191,"int", BYTE_SIZE.ONE_BYTE),
    ANONYMOUS_CHAT("ancht",         192,"int", BYTE_SIZE.ONE_BYTE),
   // FACEBOOK("fb",                  193, BYTE_SIZE.UNDEFINED),
   // TWITTER("twtr",                 194, BYTE_SIZE.UNDEFINED),
   // FACEBOOK_VALIDITY("fbvld",      195, BYTE_SIZE.ONE_BYTE),
   // TWITTER_VALIDITY("twtrvld",     196, BYTE_SIZE.ONE_BYTE),
    LAST_ONLINE_TIME("lot",         197,"long", BYTE_SIZE.EIGHT_BYTES),
    MATCH_BY("mb",                  198,"int", BYTE_SIZE.ONE_BYTE),
    SETTINGS_NAME("sn",             199, "int", BYTE_SIZE.FOUR_BYTES),
    SETTINGS_VALUE("sv",            200, "int", BYTE_SIZE.FOUR_BYTES),
    SEQUENCE("seq",                 202, "string", BYTE_SIZE.UNDEFINED),
    IS_DIVERTED_CALL("idc",         203 ,"bool",BYTE_SIZE.ONE_BYTE),
    APPLICATION_TYPE("apt",         204 ,"int",BYTE_SIZE.FOUR_BYTES),
    IS_PICKED_FROM_PHONE("ispc",    205,"int",BYTE_SIZE.FOUR_BYTES),
    IS_PICKED_FROM_EMAIL("isepc",    206,"int",BYTE_SIZE.FOUR_BYTES),
    USER_DETAILS("userDetails",      207,"byte", BYTE_SIZE.UNDEFINED),
    IS_ACTIVE("acvt",               208,"int",BYTE_SIZE.FOUR_BYTES),
    IS_MUTUAL("mtl",                209,"bool",BYTE_SIZE.ONE_BYTE),
    INFORMATION_TYPE("itp",         210,"int",BYTE_SIZE.FOUR_BYTES),
    NO_OF_MUTUAL_FRIEND("nmf",       211,"int",BYTE_SIZE.FOUR_BYTES),
    USER_IDENTITY_LIST("idlist",    213,"long", BYTE_SIZE.EIGHT_BYTES),
    INDEX_OF_HEADERS("inOH",        215,"int", BYTE_SIZE.FOUR_BYTES),
    SEARCH_CONTACT("searchedcontactlist",        216,"byte", BYTE_SIZE.UNDEFINED),
    CALL_LIST("callList",        217,"byte", BYTE_SIZE.UNDEFINED),
    SENT_TIME("senttime",        218,"long", BYTE_SIZE.EIGHT_BYTES),
    CALLEE_ID("calleeID",        219,"long", BYTE_SIZE.EIGHT_BYTES),
    CALLER_ID("callerID",        220,"long", BYTE_SIZE.EIGHT_BYTES),
    COUNT("count",               221,"int", BYTE_SIZE.FOUR_BYTES),
    CONTACT_IDS("contactIds",    224,"long", BYTE_SIZE.EIGHT_BYTES),

    
    
    ;
    private final int id;
    private final String name;
    private final BYTE_SIZE size;
    private final String type;
    
    private static final Map<Integer, ATTRIBUTE_CODE_INFO> lookup = new HashMap<Integer, ATTRIBUTE_CODE_INFO>();

    static {
        for (ATTRIBUTE_CODE_INFO attribute_code_info : ATTRIBUTE_CODE_INFO.values()) {
            lookup.put(attribute_code_info.getId(), attribute_code_info);
        }
    }

    
    private ATTRIBUTE_CODE_INFO(String name, int id, String type, BYTE_SIZE size ) {
        this.id = id;
        this.name = name;
        this.size = size;
        this.type = type;
    }
    
  


    public static ATTRIBUTE_CODE_INFO get(int id) {
        return lookup.get(id);
    
    }
    
    public String getName(){
        return name;
    }
    
    public int getId(){
        return id;
    }
    
    public BYTE_SIZE getSize(){
        return size;
    }

    public String getType() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return type;
    }
}