package org.ipvision.jsonBuilder;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author alamgir
 */
public enum BYTE_SIZE {
    UNDEFINED(0),
    ONE_BYTE(1),
    TWO_BYTES(2),
    FOUR_BYTES(4),
    SIX_BYTES(6),
    EIGHT_BYTES(8);
    
    private final int size;
    private BYTE_SIZE(int size) {
        this.size = size;
    }
    
    @Override
    public String toString(){
        return String.valueOf(size);
    }
    
    public int getValue(){
        return size;
    }
    
}
