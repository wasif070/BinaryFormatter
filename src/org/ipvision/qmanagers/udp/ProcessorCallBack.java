/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.qmanagers.udp;

import java.net.DatagramPacket;
import org.ipvision.parsers.RelayPacketAttributes;

/**
 *
 * @author saikat
 */
public interface ProcessorCallBack {
    public DatagramPacket processPacket();
    public RelayPacketAttributes processRelayPacket();
}
