/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.qmanagers.udp;

import java.net.DatagramPacket;
import org.ipvision.byteparser.RelayPacketParser;
import org.ipvision.parsers.RelayPacketAttributes;
import org.ipvision.qmanagers.helpers.ReceiveBrokenPacket;
import org.ipvision.qmanagers.helpers.ReceiveBrokenPacketFromRelay;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author saikat
 */
public class PacketProcessor implements ProcessorCallBack {

    private DatagramPacket packet;

    public PacketProcessor(DatagramPacket packet) {
        this.packet = packet;
    }

    @Override
    public DatagramPacket processPacket() {
        byte[] data = packet.getData();
        if (data[1] == 0 || data[1] == 2) {
            return packet;
        }

        ReceiveBrokenPacket brokenPacket = new ReceiveBrokenPacket(packet);
        return brokenPacket.process();
    }

    public RelayPacketAttributes processRelayPacket() {
        byte[] received_bytes = new byte[packet.getLength()];
        System.arraycopy(packet.getData(), 0, received_bytes, 0, packet.getLength());

        RelayPacketAttributes attributes = RelayPacketParser.parsePacket(received_bytes, 0);

        if (attributes.getIsDevidedPacket() == AppConstants.YES) {
            ReceiveBrokenPacketFromRelay brokenPacket = new ReceiveBrokenPacketFromRelay(packet, attributes);
            attributes =  brokenPacket.process();
        } 
        return attributes;
    }

}
