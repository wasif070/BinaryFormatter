/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.qmanagers.helpers;

import java.io.IOException;
import java.net.DatagramPacket;
import java.util.concurrent.ConcurrentHashMap;
import org.ipvision.byteBuilder.PacketBuilder;
import org.ringid.utilities.AttributeCodes;
import org.ipvision.parsers.BrokenPacketAttributes;
import org.ipvision.parsers.RelayPacketAttributes;
import org.ipvision.byteparser.BrokenPacketParser;
import org.ipvision.repositories.PacketsRepository;
import org.ipvision.threads.UDPTransport;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Ashraful
 */
public class ReceiveBrokenPacketFromRelay {

    private DatagramPacket packet;
    private RelayPacketAttributes relay_attributes;

    public ReceiveBrokenPacketFromRelay(DatagramPacket p_packet, RelayPacketAttributes p_attributes) {
        packet = p_packet;
        relay_attributes = p_attributes;
    }

    public RelayPacketAttributes process() {
        if (relay_attributes != null && relay_attributes.getServerPacketID() > 0) {
            try {
                PacketBuilder confirmaitioBuilder = new PacketBuilder()
                        .addInt(AttributeCodes.ACTION, AppConstants.ACTION_CONFIRMATION, 2)
                        .addLong(AttributeCodes.SERVER_PACKET_ID, relay_attributes.getServerPacketID(), 6);
                byte[] send_bytes = confirmaitioBuilder.getByteContents();
                DatagramPacket confirmation_packet = new DatagramPacket(send_bytes, send_bytes.length, packet.getAddress(), packet.getPort());
                UDPTransport.getInstance().send_relay(confirmation_packet);

            } catch (IOException ioEx) {
//                FnFLogger.getInstance(1).error("IOException in sending confirmation of broken packet-->", ioEx);
                ioEx.printStackTrace();
            }
        }

        byte[] recv_bytes = relay_attributes.getData();
        long user_id = relay_attributes.getUserID();

        BrokenPacketAttributes attributes = BrokenPacketParser.parseAsClient(recv_bytes, 0);

        int total_packets = attributes.getTotalPacket();
        int packet_number = attributes.getPacketNumber();
        String key = user_id + "" + attributes.getServerUniqueKey();

        byte data_bytes[] = attributes.getData();
        ConcurrentHashMap<Integer, byte[]> packet_list = PacketsRepository.getInstance().getBrokenPacketList(key);
        if (packet_list != null) {
            packet_list.put(packet_number, data_bytes);
            PacketsRepository.getInstance().putBrokenPacketList(key, packet_list);
            PacketsRepository.getInstance().getTimeMap().put(key, System.currentTimeMillis());
        } else {
            packet_list = new ConcurrentHashMap<>();
            packet_list.put(packet_number, data_bytes);
            PacketsRepository.getInstance().putBrokenPacketList(key, packet_list);
        }

        if (total_packets == packet_list.size()) {
            int total_read = 0;
            for (byte[] bytes : packet_list.values()) {
                total_read += bytes.length;
            }
            byte[] all_data = new byte[total_read];

            total_read = 0;
            for (int i = 0; i < packet_list.size(); i++) {
                try {
                    byte[] data = packet_list.get(i);
                    System.arraycopy(data, 0, all_data, total_read, data.length);
                    total_read += data.length;
                } catch (Exception ex) {
//                    FnFLogger.getInstance(1).error("Exception in isDevidedPacket", ex);
                }
            }

            PacketsRepository.getInstance().removeBrokenPacketList(key);
            PacketsRepository.getInstance().getTimeMap().remove(key);

            relay_attributes.setData(all_data);
            return relay_attributes;
        }
        return null;
    }
}
