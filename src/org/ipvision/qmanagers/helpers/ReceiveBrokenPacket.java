/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.qmanagers.helpers;

import java.io.IOException;
import java.net.DatagramPacket;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ipvision.byteBuilder.PacketBuilder;
import org.ipvision.byteBuilder.exceptions.HeaderMissingException;
import org.ringid.utilities.AttributeCodes;
import org.ipvision.parsers.BrokenPacketAttributes;
import org.ipvision.byteparser.BrokenPacketParser;
import org.ipvision.parsers.CommonPacketBuilder;
import org.ipvision.repositories.PacketsRepository;
import org.ipvision.threads.UDPTransport;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author Ashraful
 */
public class ReceiveBrokenPacket {

    private DatagramPacket packet;
    private int handler;
    private int port;

    public ReceiveBrokenPacket(DatagramPacket p_packet, int p_handler, int p_port) {
        packet = p_packet;
        handler = p_handler;
        port = p_port;
    }

    public ReceiveBrokenPacket(DatagramPacket packet) {
        this.packet = packet;
    }

    public DatagramPacket process() {
        byte[] recv_bytes = new byte[packet.getLength()];
        System.arraycopy(packet.getData(), 0, recv_bytes, 0, packet.getLength());

        BrokenPacketAttributes brokenPacketDTO = BrokenPacketParser.parseAsServer(recv_bytes, 2);

        int total_packets = brokenPacketDTO.getTotalPacket();
        String packet_id = brokenPacketDTO.getClientPacketID();
        sendConfirmation(packet, packet_id, brokenPacketDTO.getWebKey(), brokenPacketDTO.getTabID());

        int packet_number = brokenPacketDTO.getPacketNumber();
        String key = brokenPacketDTO.getClientUniqueKey();

        byte data_bytes[] = brokenPacketDTO.getData();
        //FnFLogger.getInstance(1).debug(key + "(" + packet_number + " of " + total_packets + ")-->" + new String(data_bytes));
        ConcurrentHashMap<Integer, byte[]> packet_list = PacketsRepository.getInstance().getBrokenPacketList(key);
        if (packet_list != null) {
            packet_list.put(packet_number, data_bytes);
            PacketsRepository.getInstance().putBrokenPacketList(key, packet_list);
            PacketsRepository.getInstance().getTimeMap().put(key, System.currentTimeMillis());
        } else {
            packet_list = new ConcurrentHashMap<>();
            packet_list.put(packet_number, data_bytes);
            PacketsRepository.getInstance().putBrokenPacketList(key, packet_list);
        }

        if (total_packets == packet_list.size()) {
            int total_read = 0;
            for (byte[] bytes : packet_list.values()) {
                total_read += bytes.length;
            }
            byte[] all_data = new byte[total_read + 2];//2 bytes header added for same data format

            total_read = 2;
            for (int i = 0; i < packet_list.size(); i++) {
                try {
                    byte[] data = packet_list.get(i);
                    System.arraycopy(data, 0, all_data, total_read, data.length);
                    total_read += data.length;
                } catch (Exception ex) {
//                    FnFLogger.getInstance(1).error("Exception in isDevidedPacket-->", ex);
                }
            }
            PacketsRepository.getInstance().removeBrokenPacketList(key);
            PacketsRepository.getInstance().getTimeMap().remove(key);

            packet.setData(all_data);
            packet.setLength(total_read);

            return packet;

//            switch (handler) {
//                case AppConstants.AUTH_HANDLER: {
//                    AuthHandler authHandler = new AuthHandler(packet, port);
//                    ThreadExecutor.getInstance().execute(authHandler);
//                    break;
//                }
//                case AppConstants.REQUEST_HANDLER: {
//                    RequestHandler requestHandler = new RequestHandler(packet, port);
//                    ThreadExecutor.getInstance().execute(requestHandler);
//                    break;
//                }
//                case AppConstants.UPDATE_HANDLER: {
//                    UpdateHandler updateHandler = new UpdateHandler(packet, port);
//                    ThreadExecutor.getInstance().execute(updateHandler);
//                    break;
//                }
//                case AppConstants.CHAT_HANDLER: {
//                    ChatHandler chatHandler = new ChatHandler(packet, port);
//                    ThreadExecutor.getInstance().execute(chatHandler);
//                    break;
//                }
//            }
        }
        return null;
    }

    private void sendConfirmation(DatagramPacket packet, String packet_id, String web_key, int tab_id) {
        try {
            PacketBuilder builder = new PacketBuilder()
                    .addInt(AttributeCodes.ACTION, AppConstants.ACTION_CONFIRMATION, 2)
                    .addString(AttributeCodes.CLIENT_PACKET_ID, packet_id)
                    .addString(AttributeCodes.WEB_UNIQUE_KEY, web_key)
                    .addInt(AttributeCodes.WEB_TAB_ID, tab_id, 1);

            byte send_bytes[] = builder.getByteContents();
            send_bytes[0] = (byte) AppConstants.NORMAL_JSON;
            DatagramPacket packet_from_server = new DatagramPacket(send_bytes, send_bytes.length, packet.getAddress(), packet.getPort());
            UDPTransport.getInstance().send(packet_from_server, port);
        } catch (IOException ioEx) {
            ioEx.printStackTrace();
        }
    }

    private void sendConfirmationByte(DatagramPacket packet, String packet_id, String web_key, int tab_id) {
        PacketBuilder builder = new PacketBuilder();
        builder.setWebUniqueKey(web_key);
        builder.makePacket(AppConstants.ACTION_CONFIRMATION, 0, packet.getAddress(), packet.getPort(), port, packet_id, tab_id, null, 0, false);
    }
}
