/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.byteBuilder;
import org.ipvision.byteBuilder.exceptions.HeaderMissingException;
import org.ringid.utilities.AppConstants;

/**
 *
 * @author alamgir
 * @param <T>
 */
public abstract class ByteBuilder<T extends ByteBuilder<T>> {
    private int _length = 0;
    private byte[] send_bytes = new byte[AppConstants.TWO_KB]  ;
    private final boolean isBrokenPacketHeaderBuilder;
    
    public ByteBuilder(boolean isBroken, boolean addCheckByte) {
        this.isBrokenPacketHeaderBuilder = isBroken;
        //this.send_bytes = new byte[2048];
        if(addCheckByte){
            if(isBroken){
                send_bytes[ 0 ] = AppConstants.BROKEN_BYTES;
            }else{
                send_bytes[ 0 ] = AppConstants.NORMAL_BYTES;
            }
             _length ++;
        }
       
    }
    public boolean isBrokenPacket(){
        return isBrokenPacketHeaderBuilder;
    }
    protected void setResizeSendBytes(byte[] data){
        
        this.send_bytes = data;
        this._length = data.length;
    }
    
    public T addInt(int attribute, int value, int length) {
        if (value < 0) {
            return (T)this;
        }
        setAttributeCode(attribute);

        send_bytes[_length++] = (byte) (length);

        switch (length) {
            case 1: {
                send_bytes[_length++] = (byte) (value);
                break;
            }
            case 2: {
                send_bytes[_length++] = (byte) (value >> 8);
                send_bytes[_length++] = (byte) (value);
                break;
            }
            case 4: {
                send_bytes[_length++] = (byte) (value >> 24);
                send_bytes[_length++] = (byte) (value >> 16);
                send_bytes[_length++] = (byte) (value >> 8);
                send_bytes[_length++] = (byte) (value);
                break;
            }
        }

        return (T)this;
    }

    public T addLong(int attribute, long value, int length) {
        if (value < 0) {
            return (T)this;
        }
        setAttributeCode(attribute);

        send_bytes[_length++] = (byte) length;

        switch (length) {
            case 6: {
                send_bytes[_length++] = (byte) (value >> 40);
                send_bytes[_length++] = (byte) (value >> 32);
                send_bytes[_length++] = (byte) (value >> 24);
                send_bytes[_length++] = (byte) (value >> 16);
                send_bytes[_length++] = (byte) (value >> 8);
                send_bytes[_length++] = (byte) (value);
                break;
            }
            case 8: {
                send_bytes[_length++] = (byte) (value >> 56);
                send_bytes[_length++] = (byte) (value >> 48);
                send_bytes[_length++] = (byte) (value >> 40);
                send_bytes[_length++] = (byte) (value >> 32);
                send_bytes[_length++] = (byte) (value >> 24);
                send_bytes[_length++] = (byte) (value >> 16);
                send_bytes[_length++] = (byte) (value >> 8);
                send_bytes[_length++] = (byte) (value);
                break;
            }
        }
        return (T)this;
    }

    public T addString(int attribute, String value) {
        if (value == null || value.length() == 0) {
            return (T)this;
        }
        setAttributeCode(attribute);

        byte[] str_bytes = value.getBytes();
        int length = str_bytes.length;
        send_bytes[_length++] = (byte) length;

        System.arraycopy(str_bytes, 0, send_bytes, _length, length);
        _length += length;

        return (T)this;
    }

    public T addDouble(int code, double value) {
        String strValue = String.valueOf(value);
        addString(code, strValue);
        return (T)this;
    }
    
    public T addBigString(int attribute, String value) {
        if (value == null || value.length() == 0) {
            return (T)this;
        }
        
        setAttributeCode(attribute);

        byte[] str_bytes = value.getBytes();
        int length = str_bytes.length;

        send_bytes[_length++] = (byte) (length >> 8);
        send_bytes[_length++] = (byte) length;

        System.arraycopy(str_bytes, 0, send_bytes, _length, length);
        _length += length;

        return (T)this;
    }

    public T addBytes(int attribute, byte[] data_bytes, int src_pos, int length) {
        if (data_bytes == null || data_bytes.length == 0) {
            return (T)this;
        }
        if(attribute != 0){
            setAttributeCode(attribute);

            send_bytes[_length++] = (byte) (length >> 8);
            send_bytes[_length++] = (byte) (length);
        }
        System.arraycopy(data_bytes, src_pos, send_bytes, _length, length);
        _length += length;
        
        return (T)this;
    }

    public T addBool(int attribute, boolean value ) {
        setAttributeCode(attribute);

        send_bytes[_length++] = (byte) (1);
        send_bytes[_length++] = (byte) (value ? 1 : 0);

        return (T)this;
    }

    private void setAttributeCode(int attribute){
        if(!isBrokenPacketHeaderBuilder){
            send_bytes[_length++] = (byte) (attribute >> 8);
        }
        send_bytes[_length++] = (byte) attribute;
    }
    
    public int size(){
        return _length;
    }
    
    public byte[] getByteContents(){
        byte[] bytes = new byte[size()];
        System.arraycopy(send_bytes, 0, bytes, 0, size());
        return bytes;
    }
    public abstract void build() throws HeaderMissingException;  
}
