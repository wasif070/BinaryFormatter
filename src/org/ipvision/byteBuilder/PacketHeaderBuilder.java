/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.byteBuilder;

import org.ipvision.byteBuilder.exceptions.HeaderMissingException;

/**
 *
 * @author alamgir
 */
public class PacketHeaderBuilder extends ByteBuilder<PacketHeaderBuilder> {

    public PacketHeaderBuilder() {
        //default attributes goes here
        super(false, true);
    }

    /**
     *
     * @param isBroken
     * @param addCheckByte
     */
    public PacketHeaderBuilder(boolean isBroken, boolean addCheckByte) {
        //default attributes goes here
        super(isBroken, addCheckByte);
    }

    public byte[] getBytes() throws HeaderMissingException {
        return getByteContents();
    }

    @Override
    public void build() throws HeaderMissingException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
