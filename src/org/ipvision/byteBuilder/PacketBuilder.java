/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.byteBuilder;

import com.google.gson.JsonObject;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ipvision.BrokenPacketInfo;
import org.ipvision.byteBuilder.exceptions.HeaderMissingException;
import org.ipvision.jsonBuilder.ByteParser;
import org.ipvision.repositories.RelayPacketsRepository;
import org.ipvision.repositories.ServerPackets;
import org.ipvision.repositories.dto.RelayPacketDTO;
import org.ipvision.threads.UDPTransport;
import org.ringid.utilities.AppConstants;
import org.ringid.utilities.AttributeCodes;

/**
 *
 * @author alamgir
 */
public class PacketBuilder extends ByteBuilder<PacketBuilder> {

    private byte[] send_bytes;
    private ArrayList<BrokenPacketInfo> brokenPacketList;
    private PacketHeaderBuilder packetHeader;
    private int actionId;
    private String clientPacketId;
    private int deviceType;
    private String webUniqueKey;
    private int webTabId;
    private boolean hasBrokenPacketList;
    private boolean success;
    private int serverPacketId;
    private int uniqueKey;
    private boolean isRelay;

    public PacketBuilder() {
        super(false, false);
        uniqueKey = ServerPackets.getInstance().getUniqueKey();
        serverPacketId = ServerPackets.getInstance().getServerPacketID();
    }

    public PacketBuilder(boolean isRelay, boolean addCheckByte) {
        super(isRelay, addCheckByte);
        this.isRelay = isRelay;
    }

    public PacketHeaderBuilder getPacketHeader() throws HeaderMissingException {

        if (packetHeader == null) {
            throw new HeaderMissingException();
        }
        return packetHeader;
    }

    public PacketBuilder setPacketHeader(PacketHeaderBuilder packetHeaderBuilder) {
        this.packetHeader = packetHeaderBuilder;
        return this;
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public void setServerPacketId(int serverPacketId) {
        this.serverPacketId = this.serverPacketId;
    }

    public String getClientPacketId() {
        return clientPacketId;
    }

    public void setClientPacketId(String clientPacketId) {
        this.clientPacketId = clientPacketId;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public String getWebUniqueKey() {
        return webUniqueKey;
    }

    public void setWebUniqueKey(String webUniqueKey) {
        this.webUniqueKey = webUniqueKey;
    }

    public int getWebTabId() {
        return webTabId;
    }

    public void setWebTabId(int webTabId) {
        this.webTabId = webTabId;
    }

    public PacketBuilder setSuccess(boolean success) {
        this.success = success;
        return this;
    }

    public boolean getSuccess() {
        return this.success;
    }

    public int getPacketLength() {
        return getHeaderLength() + getDataLength();
    }

    public int getHeaderLength() {
        if (this.packetHeader == null) {
            return 0;
        }
        return this.packetHeader.size();
    }

    public int getDataLength() {
        return super.size();
    }

    public byte[] getPacketBytes() throws HeaderMissingException {
        if (send_bytes == null) {
            build();
        }
        return send_bytes;
    }

    public int getServerPacketId() {
        return serverPacketId;
    }

    public ArrayList<BrokenPacketInfo> getBrokenPackets() throws HeaderMissingException {
        if (brokenPacketList == null) {
            build();
        }
        return brokenPacketList;
    }

    public int getUniqueKey() {
        return uniqueKey;
    }

    public boolean hasBrokenPacketList() {
        try {
            if (brokenPacketList == null) {
                build();
            }
        } catch (Exception ex) {

        }
        return hasBrokenPacketList;
    }

    @Override
    public void build() throws HeaderMissingException {

        if (packetHeader == null) {
            packetHeader = new PacketHeaderBuilder();
            packetHeader.addInt(AttributeCodes.ACTION, getActionId(), 2);
            packetHeader.addLong(AttributeCodes.SERVER_PACKET_ID, serverPacketId, 8);
            packetHeader.addString(AttributeCodes.CLIENT_PACKET_ID, getClientPacketId());

            if (getDeviceType() == AppConstants.WEB) {
                packetHeader.addString(AttributeCodes.WEB_UNIQUE_KEY, getWebUniqueKey());
                packetHeader.addInt(AttributeCodes.WEB_TAB_ID, getWebTabId(), 1);
            }
        }
        if (!isRelay) {
            packetHeader.addBool(AttributeCodes.SUCCESS, getSuccess());
        }

        send_bytes = new byte[getPacketLength()];

        System.arraycopy(packetHeader.getBytes(), 0, send_bytes, 0, getHeaderLength());
        System.arraycopy(getByteContents(), 0, send_bytes, getHeaderLength(), getDataLength());

        int length = getPacketLength();
        if (length >= AppConstants.BROKEN_PACKET_DATA_SIZE) {
            hasBrokenPacketList = true;
            brokenPacketList = new BrokenPacketBuilder().getBrokenPackets();
        }
    }

    @Override
    public int size() {
        return getPacketLength();
    }

    private class BrokenPacketHeaderBuilder extends ByteBuilder<BrokenPacketHeaderBuilder> {

        public BrokenPacketHeaderBuilder() {
            //default attributes goes here
            super(true, true);
        }

        public byte[] getBytes() throws HeaderMissingException {
            return getByteContents();
        }

        @Override
        public void build() throws HeaderMissingException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    private class BrokenPacketBuilder {

        private final BrokenPacketHeaderBuilder brokenPacketHeader;
        private final int total_packets;

        public BrokenPacketBuilder() {
            brokenPacketHeader = new BrokenPacketHeaderBuilder();
            brokenPacketHeader.
                    addInt(AttributeCodes.ACTION, getActionId(), 2);

            if (getDeviceType() == AppConstants.I_PHONE) {
                brokenPacketHeader.
                        addString(AttributeCodes.CLIENT_PACKET_ID, getClientPacketId());
            } else if (getDeviceType() == AppConstants.WEB) {
                brokenPacketHeader.
                        addString(AttributeCodes.WEB_UNIQUE_KEY, getWebUniqueKey()).
                        addInt(AttributeCodes.WEB_TAB_ID, getWebTabId(), 1);
            }

            brokenPacketHeader.addInt(AttributeCodes.UNIQUE_KEY, getUniqueKey(), 4);
            total_packets = (getPacketLength() / AppConstants.BROKEN_PACKET_DATA_SIZE) + (getPacketLength() % AppConstants.BROKEN_PACKET_DATA_SIZE > 0 ? 1 : 0);
            int byteSize = total_packets > 128 ? 2 : 1;
            brokenPacketHeader.addInt(AttributeCodes.TOTAL_PACKETS, total_packets, byteSize);
        }

        public ArrayList<BrokenPacketInfo> getBrokenPackets() throws HeaderMissingException {
            ArrayList<BrokenPacketInfo> brokenList = new ArrayList<>();

            int dataAttributeByteSize = 3;
            int packetNo = 1;
            //int dataStartPos = 0;
            int dataSize = AppConstants.BROKEN_PACKET_DATA_SIZE;

            int packetByteStartIndex = 1;

            while (packetNo <= total_packets) {
                int byteSize = total_packets > 128 ? 2 : 1;

                BrokenPacketInfo brokenPacketInfo = new BrokenPacketInfo();

                int serverPacketId = ServerPackets.getInstance().getServerPacketID();
                brokenPacketInfo.setServerPacketId(serverPacketId);

                brokenPacketHeader.addLong(AttributeCodes.SERVER_PACKET_ID, serverPacketId, 8);
                BrokenHeaderAttributeBuilder extraAttributes = new BrokenHeaderAttributeBuilder().
                        addInt(AttributeCodes.PACKET_NUMBER, packetNo - 1, byteSize);

                byte[] send_bytes = new byte[brokenPacketHeader.size() + dataSize + dataAttributeByteSize + extraAttributes.size()];
                System.arraycopy(brokenPacketHeader.getBytes(), 0, send_bytes, 0, brokenPacketHeader.size());
                System.arraycopy(extraAttributes.getBytes(), 0, send_bytes, brokenPacketHeader.size(), extraAttributes.size());

                this.addBytes(AttributeCodes.DATA, getPacketBytes(), packetByteStartIndex, send_bytes, brokenPacketHeader.size() + extraAttributes.size(), dataSize);

                brokenPacketInfo.setBrokenPacketData(send_bytes);
                brokenList.add(brokenPacketInfo);
                packetByteStartIndex += dataSize;

                int remainingDataSize = (getPacketLength() - 1) - packetNo * AppConstants.BROKEN_PACKET_DATA_SIZE;
                dataSize = remainingDataSize < AppConstants.BROKEN_PACKET_DATA_SIZE ? remainingDataSize : AppConstants.BROKEN_PACKET_DATA_SIZE;

                packetNo++;
            }
            return brokenList;
        }

        public void addBytes(int attribute, byte[] data_bytes, int src_pos, byte[] send_bytes, int dest_start, int length) {
            send_bytes[dest_start++] = (byte) attribute;

            send_bytes[dest_start++] = (byte) (length >> 8);
            send_bytes[dest_start++] = (byte) (length);

            System.arraycopy(data_bytes, src_pos, send_bytes, dest_start, length);
        }
    }

    private class BrokenHeaderAttributeBuilder extends ByteBuilder<BrokenHeaderAttributeBuilder> {

        public BrokenHeaderAttributeBuilder() {
            //default attributes goes here
            super(true, false);
        }

        public byte[] getBytes() throws HeaderMissingException {
            return getByteContents();
        }

        @Override
        public void build() throws HeaderMissingException {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

//    public JsonObject getJson() throws HeaderMissingException{
//        return parseJson();
//    }
//    private JsonObject parseJson() throws HeaderMissingException {
//
//        if (hasBrokenPacketList) {
//            return new ByteParser(this.getBrokenPackets()).getJsonObject();
//        }
//
//        return new ByteParser(this.getPacketBytes(), size()).getJsonObject();
//
//        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
    public Object getValue(int attributeCode) throws HeaderMissingException {
        if (hasBrokenPacketList) {

            ArrayList<byte[]> brokenPacketDataList = new ArrayList<>();
            for (BrokenPacketInfo brokenPacketInfo : getBrokenPackets()) {
                brokenPacketDataList.add(brokenPacketInfo.getBrokenPacketData());
            }
            return new ByteParser(brokenPacketDataList).getValueByAction(attributeCode);
        }

        return new ByteParser(this.getPacketBytes(), size()).getValueByAction(attributeCode);

    }

    public void setByteContents(byte[] data) {
        setResizeSendBytes(data);
    }

    public DatagramPacket makePacket(int action, int device, InetAddress address, int port, int transaction_port, String client_packet_id, int tab_id, String session_id, long userId, boolean isStorable) {
        setActionId(action);
        setClientPacketId(client_packet_id);
        setDeviceType(device);
        if (device == AppConstants.WEB) {
            setWebTabId(tab_id);
            setWebUniqueKey(session_id);
        }
        setWebUniqueKey(session_id);
        DatagramPacket dgp = null;
        try {
            if (hasBrokenPacketList()) {
                int sequence = 1;
                for (BrokenPacketInfo brokenPacketInfo : getBrokenPackets()) {
                    dgp = new DatagramPacket(brokenPacketInfo.getBrokenPacketData(), brokenPacketInfo.getBrokenPacketData().length, address, port);
                    sendPacket(dgp, transaction_port);
                    ServerPackets.getInstance().storeBrokenServerPacket(dgp, this.getUniqueKey(), userId, action, brokenPacketInfo.getServerPacketId(), sequence++, session_id);
                }
            } else {
                dgp = new DatagramPacket(getPacketBytes(), size(), address, port);
                sendPacket(dgp, transaction_port);
                if (isStorable) {
                    if (client_packet_id == null || client_packet_id.isEmpty()) {
                        ServerPackets.getInstance().storeServerPacket(dgp, userId, action, serverPacketId, session_id);
                    } else {
                        ServerPackets.getInstance().storeServerPacket(dgp, userId, action, serverPacketId, client_packet_id, 0, session_id);
                    }
                }
            }
        } catch (HeaderMissingException ex) {
            ex.printStackTrace();
        }
        return dgp;
    }

    public DatagramPacket makeRelayPacket(InetAddress address, int relayPort, long server_packet_id) {
        DatagramPacket relay_packet = null;
        try {
            if (hasBrokenPacketList) {
                for (BrokenPacketInfo brokenPacketInfo : getBrokenPackets()) {
                    relay_packet = new DatagramPacket(brokenPacketInfo.getBrokenPacketData(), brokenPacketInfo.getBrokenPacketData().length, address, relayPort);
                    UDPTransport.getInstance().send_relay(relay_packet);
                }
            } else {
                relay_packet = new DatagramPacket(getPacketBytes(), size(), address, relayPort);
                UDPTransport.getInstance().send_relay(relay_packet);
            }
            RelayPacketDTO sent_packet = new RelayPacketDTO();
            sent_packet.setPacket(relay_packet);
            sent_packet.setSentTime(System.currentTimeMillis());
            RelayPacketsRepository.getInstance().putRelayPacket(server_packet_id, sent_packet);
        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (HeaderMissingException ex) {
            ex.printStackTrace();
        }
        return relay_packet;
    }

    public static void sendPacket(DatagramPacket datagramPacket, int port) {
        try {
            UDPTransport.getInstance().send(datagramPacket, port);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
