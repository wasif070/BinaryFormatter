/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.byteBuilder;

import org.ipvision.byteBuilder.exceptions.HeaderMissingException;

/**
 *
 * @author alamgir
 */
public class PacketRecordBuilder extends ByteBuilder<PacketRecordBuilder> {

    private int attributeCode;
    private byte[] record_bytes;

    public void setAttributeCode(int attributeCode) {
        this.attributeCode = attributeCode;
    }

    public int getAttributeCode() {
        return attributeCode;
    }

    public PacketRecordBuilder() {
        //default attributes goes here
        super(false, false);
    }

    public byte[] getBytes() throws HeaderMissingException {
        return getByteContents();

        //return record_bytes;
    }

    public int getDataLength() {
        return super.size();
    }

    @Override
    public void build() throws HeaderMissingException {
        record_bytes = new byte[super.size()];
        System.arraycopy(getByteContents(), 0, record_bytes, 0, getDataLength());
        setResizeSendBytes(record_bytes);
    }

}
