/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.byteBuilder.exceptions;

/**
 *
 * @author alamgir
 */
public class HeaderMissingException extends Exception{
    public HeaderMissingException(String message) {
        super(message);
    }
    public HeaderMissingException() {
        this("Packet header missing.");
    }
    
}
