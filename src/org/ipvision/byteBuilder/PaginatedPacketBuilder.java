/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.byteBuilder;

import java.util.ArrayList;
import org.ipvision.byteBuilder.exceptions.HeaderMissingException;
import org.ipvision.repositories.ServerPackets;
import org.ringid.utilities.AppConstants;
import org.ringid.utilities.AttributeCodes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alamgir
 */
public class PaginatedPacketBuilder {

    private ArrayList<PacketBuilder> packetList;
    private ArrayList<PacketRecordBuilder> recordList;

    private int recordsPerPacket;
    private int actionId;
    private String clientPacketId;
    private int deviceType;
    private String webUniqueKey;
    private int webTabId;

    private final Logger logger = LoggerFactory.getLogger(PaginatedPacketBuilder.class);

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public String getClientPacketId() {
        return clientPacketId;
    }

    public void setClientPacketId(String clientPacketId) {
        this.clientPacketId = clientPacketId;
    }

    public int getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(int deviceType) {
        this.deviceType = deviceType;
    }

    public String getWebUniqueKey() {
        return webUniqueKey;
    }

    public void setWebUniqueKey(String webUniqueKey) {
        this.webUniqueKey = webUniqueKey;
    }

    public int getWebTabId() {
        return webTabId;
    }

    public void setWebTabId(int webTabId) {
        this.webTabId = webTabId;
    }

    public int getRecordsPerPacket() {
        return recordsPerPacket;
    }

    public void setRecordsPerPacket(int recordsPerPacket) {
        this.recordsPerPacket = recordsPerPacket;
    }

    public void setRecordList(ArrayList<PacketRecordBuilder> recordList) {
        this.recordList = recordList;
    }

    public ArrayList<PacketBuilder> getPacketList() throws HeaderMissingException {
        if (packetList == null) {
            build();
        }
        return packetList;
    }

    private void build() throws HeaderMissingException {
        packetList = new ArrayList<>();
        if (recordList == null || recordList.isEmpty()) {
            PacketBuilder builder = new PacketBuilder();
//            builder.setPacketHeader(
//                    new PacketHeaderBuilder()
//                    .addInt(AttributeCodes.ACTION, getActionId(), 2)
//                    .addString(AttributeCodes.CLIENT_PACKET_ID, getClientPacketId())
//                    )
            builder.setSuccess(false)
                    .addString(AttributeCodes.MESSAGE, AppConstants.NO_DATA_FOUND);
//                    .build();
            packetList.add(builder);
            return;
        }

        int noOfRecords = recordList.size();
        int noOfPackets = (noOfRecords / recordsPerPacket) + (noOfRecords % recordsPerPacket == 0 ? 0 : 1);

        for (int i = 0, recordCounter = 0; i < noOfPackets; i++) {
            PacketBuilder builder = new PacketBuilder();

            builder.setActionId(actionId);
            builder.setClientPacketId(clientPacketId);
            builder.setDeviceType(deviceType);
            builder.setWebTabId(webTabId);
            builder.setWebUniqueKey(webUniqueKey);

            builder.setPacketHeader(
                    new PacketHeaderBuilder()
                    .addInt(AttributeCodes.ACTION, builder.getActionId(), 2)
                    .addString(AttributeCodes.CLIENT_PACKET_ID, builder.getClientPacketId())
                    .addLong(AttributeCodes.SERVER_PACKET_ID, builder.getServerPacketId(), 8)
            )
                    .setSuccess(true)
                    .addInt(AttributeCodes.TOTAL_RECORDS, noOfRecords, 2)
                    .addInt(AttributeCodes.PACKET_NUMBER, i + 1, 2)
                    .addInt(AttributeCodes.TOTAL_PACKETS, noOfPackets, 2);
            try {
                for (int j = 0; j < recordsPerPacket && recordCounter < recordList.size(); j++) {
                    PacketRecordBuilder record = recordList.get(recordCounter);
                    builder.addBytes(record.getAttributeCode(), record.getBytes(), 0, record.size());
                    recordCounter++;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            packetList.add(builder);
        }

    }

}
