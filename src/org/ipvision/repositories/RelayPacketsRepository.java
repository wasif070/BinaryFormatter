/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.repositories;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.ringid.utilities.AppConstants;
import org.ipvision.repositories.dto.RelayPacketDTO;

/**
 *
 * @author user
 */
public class RelayPacketsRepository {

    private static RelayPacketsRepository instance;
    private ConcurrentHashMap<Long, RelayPacketDTO> relay_packets;

    public RelayPacketsRepository() {
        relay_packets = new ConcurrentHashMap<>(AppConstants.MAP_SIZE_RELAY_PACKETS);
    }

    public static RelayPacketsRepository getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    public synchronized static void createInstance() {
        if (instance == null) {
            instance = new RelayPacketsRepository();
        }
    }

    /* RelayPacket functions*/
    public void putRelayPacket(Long packet_id, RelayPacketDTO packet) {
        relay_packets.put(packet_id, packet);
    }

    public boolean containsRelayPacket(long packet_id) {
        return relay_packets.containsKey(packet_id);
    }

    public RelayPacketDTO getRelayPacket(long packet_id) {
        return relay_packets.get(packet_id);
    }

    public void removeRelayPacket(long packet_id) {
        RelayPacketDTO relayPacketDTO = relay_packets.remove(packet_id);
        if (relayPacketDTO != null) {
            relayPacketDTO = null;
        }
    }

    public Set<Long> getKeySet() {
        return relay_packets.keySet();
    }
}
