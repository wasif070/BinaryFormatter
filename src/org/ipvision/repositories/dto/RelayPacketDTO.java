/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.repositories.dto;

import java.net.DatagramPacket;

/**
 *
 * @author Ashraful
 */
public class RelayPacketDTO {

    private DatagramPacket packet;
    private long sent_time;

    public DatagramPacket getPacket() {
        return packet;
    }

    public void setPacket(DatagramPacket packet) {
        this.packet = packet;
    }

    public long getSentTime() {
        return sent_time;
    }

    public void setSentTime(long sent_time) {
        this.sent_time = sent_time;
    }
}
