/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.repositories;

import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author user
 */
public class PacketsRepository {

    private ConcurrentHashMap<String, ConcurrentHashMap<Integer, byte[]>> received_broken_packets;
    private ConcurrentHashMap<String, Long> time_map;
    private static PacketsRepository instance;

    public static PacketsRepository getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    public synchronized static void createInstance() {
        if (instance == null) {
            instance = new PacketsRepository();
        }
    }

    /* BrokenPacketList functions*/
    public void putBrokenPacketList(String key, ConcurrentHashMap<Integer, byte[]> list) {
        received_broken_packets.put(key, list);
    }

    public ConcurrentHashMap<Integer, byte[]> getBrokenPacketList(String key) {
        return received_broken_packets.get(key);
    }

    public void removeBrokenPacketList(String key) {
        ConcurrentHashMap<Integer, byte[]> brokenPacketMap = received_broken_packets.remove(key);
        if (brokenPacketMap != null) {
            brokenPacketMap = null;
        }
    }

    public ConcurrentHashMap<String, Long> getTimeMap() {
        return time_map;
    }

}
