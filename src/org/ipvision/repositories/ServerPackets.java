/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.repositories;

import java.net.DatagramPacket;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author user
 */
public class ServerPackets {

    private static ServerPackets instance;
    private AtomicInteger integer;
    private AtomicInteger uniKey;

    public static ServerPackets getInstance() {
        if (instance == null) {
            createInstance();
        }
        return instance;
    }

    public synchronized static void createInstance() {
        if (instance == null) {
            instance = new ServerPackets();
        }
    }

    public synchronized int getServerPacketID() {
        int packetID = integer.getAndIncrement();
        if (packetID == Integer.MAX_VALUE) {
            integer.set(1);
        }
        return packetID;
    }

    public synchronized int getUniqueKey() {
        int uniqueKey = uniKey.getAndIncrement();
        if (uniqueKey == Integer.MAX_VALUE) {
            uniKey.set(1);
        }
        return uniqueKey;
    }

    public void storeServerPacket(DatagramPacket packet, long to_user_id, int action, int server_packet_id, String sess_id) {
    }

    public void storeServerPacket(DatagramPacket packet, long to_user_id, int action, int server_packet_id, String client_packet_id, int sequence, String sess_id) {

    }

    public void storeBrokenServerPacket(DatagramPacket packet, long unique_key, long to_user_id, int action, int server_packet_id, int sequence, String sess_id) {

    }

}
