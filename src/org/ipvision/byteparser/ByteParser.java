/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.byteparser;

/**
 *
 * @author saikat
 */
public abstract class ByteParser {

    private static byte[] bytes;

    public static void setBytes(byte[] bytes) {
        bytes = bytes;
    }

    public static int getInt(int length,int index) {
        if (index + length >= 5000) {
            return 0;
        }
        switch (length) {
            case 1:
                return (bytes[index++] & 0xFF);

            case 2:
                return (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
            case 4:
                return (bytes[index++] & 0xFF) << 24 | (bytes[index++] & 0xFF) << 16 | (bytes[index++] & 0xFF) << 8
                        | (bytes[index++] & 0xFF);
        }
        return 0;
    }

    public static long getLong(int length, int index) {
        if (index + length >= 5000) {
            return 0;
        }
        long result = 0;
        for (int i = (length - 1); i > -1; i--) {
            result |= (bytes[index++] & 0xFFL) << (8 * i);
        }
        return result;
    }

    public static String getString(int length, int index) {
        if (index + length >= 5000) {
            return null;
        }
        byte[] str_bytes = new byte[length];
        System.arraycopy(bytes, index, str_bytes, 0, length);
        index += length;
        return new String(str_bytes);
    }

    public static byte[] getBytes(int length, int index) {
        if (index + length >= 5000) {
            return null;
        }
        byte[] data_bytes = new byte[length];
        if (bytes.length - index >= length) {
            System.arraycopy(bytes, index, data_bytes, 0, length);
        }
        index += length;
        return data_bytes;
    }

    public static boolean getBool(int length, int index) {
        if (index + length >= 5000) {
            return false;
        }
        return (bytes[index++] != 0);
    }
}
