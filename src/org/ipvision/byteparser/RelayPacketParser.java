/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.byteparser;

import java.util.ArrayList;
import org.ipvision.parsers.RelayPacketAttributes;
import org.ringid.utilities.AttributeCodes;

/**
 *
 * @author Ashraful
 */
public class RelayPacketParser extends ByteParser{

    public static RelayPacketAttributes parsePacket(byte[] bytes, int offset) {
        RelayPacketAttributes attributes = new RelayPacketAttributes();
        setBytes(bytes);
        
        for (int index = offset; index < bytes.length; index++) {
            int attribute = (bytes[index++] & 0xFF);
            int length = 0;
            switch (attribute) {
                case AttributeCodes.ACTION: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setAction(getInt(length,index));
                    break;
                }
                case AttributeCodes.ORIGIN_ACTION: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setOriginAction(getInt(length,index));
//                    if (LogSettings.getInstance().doLog(LogSettingsConstants.LOG_RELAY)) {
//                        FnFLogger.getInstance(1).debug("Relaydata received(RelayAction:" + attributes.getAction() + ",OriginAction:" + attributes.getOriginAction() + ")-->" + new String(bytes));
//                    }
                    break;
                }
                case AttributeCodes.IS_DEVIDED_PACKET: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setIsDevidedPacket(getInt(length,index));
                    break;
                }
                case AttributeCodes.SERVER_PACKET_ID: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setServerPacketID(getInt(length,index));
                    break;
                }
                case AttributeCodes.SERVER_ID: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setServerID(getInt(length,index));
                    break;
                }
                case AttributeCodes.USER_ID: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setUserID(getLong(length,index));
                    break;
                }
                case AttributeCodes.FRIEND_ID: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setFriendID(getLong(length,index));
                    break;
                }
                case AttributeCodes.USER_NAME: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setUserIdentity(getString(length,index));
                    break;
                }
                case AttributeCodes.FULL_NAME: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setUserName(getString(length,index));
                    break;
                }
                case AttributeCodes.FRIEND_IDENTITY: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setFriendIdentity(getString(length,index));
                    break;
                }
                case AttributeCodes.FRIEND_NAME: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setFriendName(getString(length,index));
                    break;
                }
                case AttributeCodes.RING_ID: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setRingID(getLong(length, index));
                    break;
                }
                case AttributeCodes.TRANSFER_BY_ID: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setTranferByID(getLong(length, index));
                    break;
                }
                case AttributeCodes.TRANSFER_BY_IDENTITY: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setTranferByIdentity(getString(length,index));
                    break;
                }
                case AttributeCodes.TRANSFER_BY_NAME: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setTranferByName(getString(length,index));
                    break;
                }
                case AttributeCodes.DIVERTED_BY_ID: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setDivertedByID(getLong(length, index));
                    break;
                }
                case AttributeCodes.DIVERTED_BY_IDENTITY: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setDivertedByIdentity(getString(length, index));
                    break;
                }
                case AttributeCodes.DIVERTED_BY_NAME: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setDivertedByName(getString(length, index));
                    break;
                }
                case AttributeCodes.DEVICE: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setDevice(getInt(length, index));
                    break;
                }
                case AttributeCodes.DEVICE_CATEGORY: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setDeviceCategory(getInt(length, index));
                    break;
                }
                case AttributeCodes.PRESENCE: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setLiveStatus(getInt(length, index));
                    break;
                }
                case AttributeCodes.MOOD: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setMood(getInt(length, index));
                    break;
                }
                case AttributeCodes.ROOM_ID: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setRoomID(getString(length, index));
                    break;
                }
                case AttributeCodes.SHADOW_ID: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setShadowID(getLong(length, index));
                    break;
                }
                case AttributeCodes.CLIENT_PACKET_ID: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setClientPacketID(getString(length, index));
                    break;
                }
                case AttributeCodes.CALL_ID: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setCallID(getString(length, index));
                    break;
                }
                case AttributeCodes.CALL_TYPE: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setCallType(getInt(length, index));
                    break;
                }
                case AttributeCodes.SWITCH_IP: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setSwitchIP(getLong(length, index));
                    break;
                }
                case AttributeCodes.SWITCH_PORT: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setSwitchPort(getInt(length, index));
                    break;
                }
                case AttributeCodes.P2P_STATUS: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setP2PStatus(getInt(length, index));
                    break;
                }
                case AttributeCodes.COUNTRY_SHORT_CODE: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setCountryShortCode(getString(length, index));
                    break;
                }
                case AttributeCodes.IS_CELEBRITY: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setIsCelebrity(getBool(length, index));
                    break;
                }
                case AttributeCodes.REMOTE_PUSH_TYPE: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setRemotePushType(getInt(length, index));
                    break;
                }
                case AttributeCodes.USER_IDS: {
                    length = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
                    byte[] userIDBytes = getBytes(length, index);
                    ArrayList<Long> friendIDs = new ArrayList<>();
                    for (int i = 0; i < userIDBytes.length;) {
                        long result = 0;
                        for (int j = 7; j > -1; j--) {
                            result |= (userIDBytes[i++] & 0xFFL) << (8 * j);
                        }
                        friendIDs.add(result);
                    }
                    attributes.setUserIDList(friendIDs);
                    break;
                }
                case AttributeCodes.DATA: {
                    length = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
                    attributes.setData(getBytes(length, index));
                    break;
                }
                default: {
                    length = (bytes[index++] & 0xFF);
                    break;
                }
            }
            index += length - 1;
        }
        return attributes;
    }

//    private static int getInt(byte[] bytes, int index, int length) {
//        switch (length) {
//            case 1:
//                return (bytes[index++] & 0xFF);
//            case 2:
//                return (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
//            case 4:
//                return (bytes[index++] & 0xFF) << 24 | (bytes[index++] & 0xFF) << 16 | (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
//        }
//        return 0;
//    }
//
//    private static long getLong(byte[] bytes, int index, int length) {
//        long result = 0;
//        for (int i = (length - 1); i > -1; i--) {
//            result |= (bytes[index++] & 0xFFL) << (8 * i);
//        }
//        return result;
//    }
//
//    private static String getString(byte[] data_bytes, int index, int length) {
//        byte[] str_bytes = new byte[length];
//        System.arraycopy(data_bytes, index, str_bytes, 0, length);
//
//        return new String(str_bytes);
//    }
//
//    private static byte[] getBytes(byte[] received_bytes, int index, int length) {
//        byte[] data_bytes = new byte[length];
//        if (received_bytes.length - index >= length) {
//            System.arraycopy(received_bytes, index, data_bytes, 0, length);
//        }
//        return data_bytes;
//    }
//
    public static int addInt(int attribute, Integer int_value, int length, byte[] send_bytes, int index) {
        if (int_value == null || int_value.intValue() == 0) {
            return index;
        }
        int value = int_value.intValue();

        send_bytes[index++] = (byte) attribute;
        send_bytes[index++] = (byte) length;

        switch (length) {
            case 1: {
                send_bytes[index++] = (byte) (value);
                break;
            }
            case 2: {
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
            }
            case 4: {
                send_bytes[index++] = (byte) (value >> 24);
                send_bytes[index++] = (byte) (value >> 16);
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
            }
        }

        return index;
    }

    public static int addLong(int attribute, Long long_value, int length, byte[] send_bytes, int index) {
        if (long_value == null || long_value.longValue() == 0) {
            return index;
        }
        long value = long_value.longValue();

        send_bytes[index++] = (byte) attribute;
        send_bytes[index++] = (byte) length;

        switch (length) {
            case 6: {
                send_bytes[index++] = (byte) (value >> 40);
                send_bytes[index++] = (byte) (value >> 32);
                send_bytes[index++] = (byte) (value >> 24);
                send_bytes[index++] = (byte) (value >> 16);
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
            }
            case 8: {
                send_bytes[index++] = (byte) (value >> 56);
                send_bytes[index++] = (byte) (value >> 48);
                send_bytes[index++] = (byte) (value >> 40);
                send_bytes[index++] = (byte) (value >> 32);
                send_bytes[index++] = (byte) (value >> 24);
                send_bytes[index++] = (byte) (value >> 16);
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
            }
        }

        return index;
    }

    public static int addString(int attribute, String value, byte[] send_bytes, int index) {
        if (value == null || value.length() == 0) {
            return index;
        }

        send_bytes[index++] = (byte) attribute;

        byte[] id_bytes = value.getBytes();
        int length = id_bytes.length;

        send_bytes[index++] = (byte) length;
        System.arraycopy(id_bytes, 0, send_bytes, index, length);
        index += length;

        return index;
    }

    public static int addBytes(int attribute, byte[] data_bytes, int src_pos, byte[] send_bytes, int index, int length) {
        if (data_bytes == null || data_bytes.length == 0) {
            return index;
        }

        send_bytes[index++] = (byte) attribute;

        send_bytes[index++] = (byte) (length >> 8);
        send_bytes[index++] = (byte) (length);

        System.arraycopy(data_bytes, src_pos, send_bytes, index, length);
        index += length;
        return index;
    }

//    public static void main(String[] args) {
//        byte[] str = "kashdf jasjdhfuiweq hrjkwqeh riuwue rhwqkejrh wqie rhwekrjh wqkejr h".getBytes();
//        RelayPacketParser.getBytes(str, 58, 10);
//    }
}
