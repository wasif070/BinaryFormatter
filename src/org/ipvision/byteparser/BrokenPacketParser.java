/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision.byteparser;

import org.ipvision.byteparser.ByteParser;
import org.ipvision.parsers.BrokenPacketAttributes;
import org.ringid.utilities.AttributeCodes;

/**
 *
 * @author Ashraful
 */
public class BrokenPacketParser extends ByteParser{

    public static BrokenPacketAttributes parseAsServer(byte[] bytes, int offset) {
        BrokenPacketAttributes attributes = new BrokenPacketAttributes();
        setBytes(bytes);
        for (int index = offset; index < bytes.length; index++) {
            int attribute = (bytes[index++] & 0xFF);
            int length = 0;
            switch (attribute) {
                case AttributeCodes.ACTION: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setAction(getInt(length,index));
                    break;
                }
                case AttributeCodes.CLIENT_PACKET_ID: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setClientPacketID(getString(length,index));
                    break;
                }
                case AttributeCodes.TOTAL_PACKETS: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setTotalPacket(getInt(length,index));
                    break;
                }
                case AttributeCodes.PACKET_NUMBER: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setPacketNumber(getInt(length,index));
                    break;
                }
                case AttributeCodes.UNIQUE_KEY: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setClientUniqueKey(getString(length,index));
                    break;
                }
                case AttributeCodes.WEB_UNIQUE_KEY: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setWebKey(getString(length,index));
                    break;
                }
                case AttributeCodes.WEB_TAB_ID: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setTabID(getInt(length,index));
                    break;
                }
                case AttributeCodes.DATA: {
                    length = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
                    attributes.setData(getBytes(length,index));
                    break;
                }
            }
            index += length - 1;
        }

        return attributes;
    }

    public static BrokenPacketAttributes parseAsClient(byte[] bytes, int offset) {
        BrokenPacketAttributes attributes = new BrokenPacketAttributes();
        for (int index = offset; index < bytes.length; index++) {
            int attribute = (bytes[index++] & 0xFF);
            int length = 0;
            switch (attribute) {
                case AttributeCodes.ACTION: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setAction(getInt(length,index));
                    break;
                }
                case AttributeCodes.SERVER_PACKET_ID: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setServerPacketID(getInt(length,index));
                    break;
                }
                case AttributeCodes.TOTAL_PACKETS: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setTotalPacket(getInt(length,index));
                    break;
                }
                case AttributeCodes.PACKET_NUMBER: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setPacketNumber(getInt(length,index));
                    break;
                }
                case AttributeCodes.UNIQUE_KEY: {
                    length = (bytes[index++] & 0xFF);
                    attributes.setServerUniqueKey(getLong(length,index));
                    break;
                }
                case AttributeCodes.DATA: {
                    length = (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
                    attributes.setData(getBytes(length,index));
                    break;
                }
            }
            index += length - 1;
        }

        return attributes;
    }

//    private static int getInt(byte[] bytes, int index, int length) {
//        switch (length) {
//            case 1:
//                return (bytes[index++] & 0xFF);
//            case 2:
//                return (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
//            case 4:
//                return (bytes[index++] & 0xFF) << 24 | (bytes[index++] & 0xFF) << 16 | (bytes[index++] & 0xFF) << 8 | (bytes[index++] & 0xFF);
//        }
//        return 0;
//    }
//
//    private static long getLong(byte[] bytes, int index, int length) {
//        long result = 0;
//        for (int i = (length - 1); i > -1; i--) {
//            result |= (bytes[index++] & 0xFFL) << (8 * i);
//        }
//        return result;
//    }
//
//    private static String getString(byte[] data_bytes, int index, int length) {
//        byte[] str_bytes = new byte[length];
//        System.arraycopy(data_bytes, index, str_bytes, 0, length);
//
//        return new String(str_bytes);
//    }
//
//    private static byte[] getBytes(byte[] received_bytes, int index, int length) {
//        byte[] data_bytes = new byte[length];
//        System.arraycopy(received_bytes, index, data_bytes, 0, length);
//        return data_bytes;
//    }
//
    public static int addInt(int attribute, Integer int_value, int length, byte[] send_bytes, int index) {
        if (int_value == null || int_value.intValue() == 0) {
            return index;
        }
        int value = int_value.intValue();

        send_bytes[index++] = (byte) attribute;
        send_bytes[index++] = (byte) length;

        switch (length) {
            case 1: {
                send_bytes[index++] = (byte) (value);
                break;
            }
            case 2: {
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
            }
            case 4: {
                send_bytes[index++] = (byte) (value >> 24);
                send_bytes[index++] = (byte) (value >> 16);
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
            }
        }
        return index;
    }

    public static int addLong(int attribute, Long long_value, int length, byte[] send_bytes, int index) {
        if (long_value == null || long_value.longValue() == 0) {
            return index;
        }
        long value = long_value.longValue();

        send_bytes[index++] = (byte) attribute;
        send_bytes[index++] = (byte) length;

        switch (length) {
            case 6: {
                send_bytes[index++] = (byte) (value >> 40);
                send_bytes[index++] = (byte) (value >> 32);
                send_bytes[index++] = (byte) (value >> 24);
                send_bytes[index++] = (byte) (value >> 16);
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
            }
            case 8: {
                send_bytes[index++] = (byte) (value >> 56);
                send_bytes[index++] = (byte) (value >> 48);
                send_bytes[index++] = (byte) (value >> 40);
                send_bytes[index++] = (byte) (value >> 32);
                send_bytes[index++] = (byte) (value >> 24);
                send_bytes[index++] = (byte) (value >> 16);
                send_bytes[index++] = (byte) (value >> 8);
                send_bytes[index++] = (byte) (value);
                break;
            }
        }

        return index;
    }

    public static int addString(int attribute, String value, byte[] send_bytes, int index) {
        if (value == null || value.length() == 0) {
            return index;
        }

        send_bytes[index++] = (byte) attribute;

        byte[] id_bytes = value.getBytes();
        int length = id_bytes.length;

        send_bytes[index++] = (byte) length;
        System.arraycopy(id_bytes, 0, send_bytes, index, length);
        index += length;

        return index;
    }

    public static int addBytes(int attribute, byte[] data_bytes, int src_pos, byte[] send_bytes, int index, int length) {
        if (data_bytes == null || data_bytes.length == 0) {
            return index;
        }

        send_bytes[index++] = (byte) attribute;

        send_bytes[index++] = (byte) (length >> 8);
        send_bytes[index++] = (byte) (length);

        System.arraycopy(data_bytes, src_pos, send_bytes, index, length);
        index += length;
        return index;
    }
}
