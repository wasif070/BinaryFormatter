/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ipvision;

import java.util.ArrayList;

/**
 *
 * @author saikat
 */
public class BrokenPacketInfo {
    private int serverPacketId;
    private byte[] brokenPacketData;
    

    public int getServerPacketId() {
        return serverPacketId;
    }

    public void setServerPacketId(int serverPacketId) {
        this.serverPacketId = serverPacketId;
    }

    public byte[] getBrokenPacketData() {
        return brokenPacketData;
    }

    public void setBrokenPacketData(byte[] brokenPacketData) {
        this.brokenPacketData = brokenPacketData;
    }
    
    
}
